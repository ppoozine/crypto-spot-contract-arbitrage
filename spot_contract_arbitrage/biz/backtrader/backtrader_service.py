import backtrader as bt
import pandas as pd
import matplotlib.pyplot as plt
import datetime as dt
import numpy as np


class SMAStrategy(bt.Strategy):
    def __init__(self) -> None:
        self.dataclose = self.data0.close
        self.order = None
        self.buyprice = None
        self.buycomm = None

        self.sma = bt.indicators.MovingAverageSimple(self.data0, period=50)

    def next(self):
        if not self.position:
            if self.dataclose[0] > self.sma[0]:
                self.buy()
        else:
            if self.dataclose[0] < self.sma[0]:
                self.close()

    def notify_order(self, order):
        if order.status in [order.Submitted, order.Accepted]:
            return

        if order.status in [order.Completed]:
            if order.isbuy():
                self.log(
                    "Buy EXECTUED, Price: %.2f, Cost: %.2f, Comm %.2f" % (
                        order.executed.price, order.executed.value, order.executed.comm)
                )
                self.buyprice = order.executed.price
                self.buycomm = order.executed.comm
            else:
                self.log(
                    "SELL EXECUTED, Price: %.2f, Cost: %.2f, Comm: %.2f" % (
                        order.executed.price, order.executed.value, order.executed.comm)
                )
            self.bar_executed = len(self)
        elif order.status in [order.Canceled, order.Margin, order.Rejected]:
            self.log("oOrder Canceled/Margin/Rejected")
        self.order = None

    def notify_trade(self, trade):
        if not trade.isclosed:
            return
        self.log("OPERATION PROFIT, GROSS %.2f, NET %.2f" %
                 (trade.pnl, trade.pnlcomm))

    def log(self, txt, dt=None, doprint=True):
        if doprint:
            dt = dt or self.datas[0].datetime.date(0)
            print(f"{dt.isoformat()}, {txt}")


if __name__ == "__main__":
    cerebro = bt.Cerebro()
    dataframe = pd.read_csv("historical/1D/ftx_BTCUSDT.csv")
    dataframe['datetime'] = pd.to_datetime(dataframe["date"])
    dataframe.set_index("datetime", inplace=True)
    data_BTC = bt.feeds.PandasData(
        dataname=dataframe, timeframe=bt.TimeFrame.Days)
    cerebro.adddata(data_BTC)

    cerebro.addstrategy(SMAStrategy)
    cerebro.addanalyzer(bt.analyzers.SharpeRatio, _name="SharpeRatio")
    cerebro.addanalyzer(bt.analyzers.DrawDown, _name="DrawDown")

    cerebro.broker.setcash(10000.0)
    cerebro.broker.setcommission(commission=0.001)
    cerebro.addsizer(bt.sizers.PercentSizer, percents=90)
    result = cerebro.run()

    plt.switch_backend('agg')
    print("SharpeRatio: ",
          result[0].analyzers.SharpeRatio.get_analysis()["sharperatio"])
    print("DrawDown: ",
          result[0].analyzers.DrawDown.get_analysis()["max"]["drawdown"])

    cerebro.plot()
