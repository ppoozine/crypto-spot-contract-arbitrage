import csv
from datetime import datetime
import os
from dotenv import load_dotenv
import pandas as pd
import requests

load_dotenv()


class HistorialDataService:
    def __init__(self) -> None:
        self._url = os.getenv("FTX_URL")

    def fetch_data(self, base_currency: str, quote_currency: str, dir: str) -> csv:
        daily = str(60*60*24)
        start_date = datetime(2021, 1, 1).timestamp()
        historical = requests.get(
            f'{self._url.rstrip("/")}/{base_currency}/{quote_currency}/candles?resolution={daily}' +
            f'&start_time={start_date}'
        ).json()
        df = pd.DataFrame(historical["result"])
        df['date'] = pd.to_datetime(df['time']/1000, unit='s', origin='unix')
        df.drop(['startTime', 'time'], axis=1, inplace=True)
        save_path = os.path.join("historical", dir)
        if not os.path.exists(save_path):
            os.mkdir(save_path)
        df.to_csv(f"{save_path}/ftx_{base_currency}{quote_currency}.csv")
